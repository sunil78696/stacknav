import React from 'react';

import AppContainer from "./src/routes/index";

class App extends React.Component {
  render(){
    return (
      <AppContainer />
    );
  }
}
export default App;