import {createStackNavigator} from "react-navigation-stack";
import {createAppContainer} from "react-navigation";

import SplashScreen from "../screens/SplashScreen/index";
import LoginScreen from "../screens/LoginScreen/index";
import HomeScreen from "../screens/HomeScreen/index";

const MainContainer = createStackNavigator({
    Splash :  SplashScreen,
    Login : LoginScreen,
    Home : HomeScreen
},{
    initialRouteName:'Splash',
    headerMode:null
});

export default createAppContainer(MainContainer);