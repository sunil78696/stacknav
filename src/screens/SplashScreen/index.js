import React from 'react';
import {View, Text, StyleSheet, Button, TouchableOpacity} from "react-native";

export default class SplashScreen extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        return (
            <View style={styles.container}>
                <Text >I am from Splash Screen</Text>
                <TouchableOpacity 
                onPress={()=>this.props.navigation.navigate('Login')}>
                    <Text>Click to Move to Login Screen</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    }
});