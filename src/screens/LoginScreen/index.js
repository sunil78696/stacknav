import React from 'react';
import {View, Text, StyleSheet} from "react-native";
export default class LoginScreen extends React.Component {
    render(){
        return (
            <View style={styles.container}>
                <Text >I am from Login Screen</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    }
});